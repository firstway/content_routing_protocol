


import json



class Config:
    def __init__(self, fname):
        self.fname = fname
        self.routers = {}
        self.hosts = {}

    def load(self):
        _all = json.load(open(self.fname))
        self.routers = _all['router']        
        self.hosts = _all['host']

    def count_iface(self, router_id):
        router_id = str(router_id)
        if router_id not in self.routers["V"]:
            return 0
        return len(self.routers["V"][router_id]) 

    def ifaces(self, router_id):
        router_id = str(router_id)
        if router_id not in self.routers["V"]:
            return []
        return self.routers["V"][router_id][:]

    def get_interface_conf_list(self, router_id):
        ''' = [
            [('127.0.0.1', 11000), ('127.0.0.1', 21000), 'H' ],
            [('127.0.0.1', 11009), ('127.0.0.1', 21009), 'R' ]
        ]'''
        router_id = str(router_id)
        conf_list = []
        # connect to hosts
        for cf in self._parse_host_links(router_id):
            cf.append('H')
            conf_list.append(cf)
        #router to router
        for cf in self._parse_edges(router_id):
            cf.append('R')
            conf_list.append(cf)
        conf_list = sorted(conf_list)
        for cf in conf_list:
            print ">>",cf
        return conf_list

    def _iface_no_to_addr(self, rid, iface_no):
        # 20000+ routerID*100 + interface ID
        rid = int(rid)
        iface_no = int(iface_no)
        _port = 20000 + rid*100 + iface_no
        return ('127.0.0.1', _port)

    def _host_id_to_addr(self, host_id):
        host_id = int(host_id)
        _port = 10000+ host_id
        return ('127.0.0.1', _port)

    def _parse_edges(self, router_id):
        for edge in self.routers["E"]:
            # "1:1-3:1"
            r1,r2 = edge.split("-")
            r1 = r1.split(":")
            r2 = r2.split(":")
            if r1[0] == router_id:
                yield [ self._iface_no_to_addr(*r) for r in (r1,r2) ]
            elif r2[0] == router_id:
                yield [ self._iface_no_to_addr(*r) for r in (r2,r1) ]

    def _parse_host_links(self, router_id):
        router_id = int(router_id)
        for host_id, _router in self.hosts.items():
            if router_id == _router[0]:
                yield [ self._iface_no_to_addr(*_router), self._host_id_to_addr(host_id) ]
    
    def get_host_conf(self, hid):
        for host_id, _router in self.hosts.items():
            host_id =  int(host_id)
            if hid == host_id:
                return ( self._host_id_to_addr(host_id), self._iface_no_to_addr(*_router))
            


if __name__ == "__main__":
    CF = Config("topo.txt")
    CF.load()
    CF.get_interface_conf_list(1)
    print "count",CF.count_iface(1)
    print '-'*50
    CF.get_interface_conf_list(2)
    print "count",CF.count_iface(2)
    print '-'*50
    CF.get_interface_conf_list(3)
    print '-'*50
    CF.get_interface_conf_list(4)


    print 'host'*20
    print CF.get_host_conf(1)
    print CF.get_host_conf(2)
    print CF.get_host_conf(3)
