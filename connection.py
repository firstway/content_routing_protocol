

MSG_C_REQ = 0x00
MSG_C_RESP = 0x01
MSG_C_UPDATE = 0x02




class PacketHdr:
    def __init__(self):
        self.buf = [] # create new header

    def setBuf(self, s):
        self.buf = s

    def _build(self):
        if list == type(self.buf):
            self.buf = ''.join(self.buf)

    def getSize(self):
        self._build()
        return len(self.buf)

    def getData(self):
        self._build()
        return self.buf

    def getOctet(self, position):
        return ord(self.buf[position])

    def setOctet(self, position, i):
        if type('') == type(self.buf):
            self.buf = [c for c in self.buf]
        self.buf[position] = chr(i)

    def getIntegerInfo(self, position):
        val = 0
        val = ord(self.buf[position])
        val = (val << 8) | ord(self.buf[position+1])
        val = (val << 8) | ord(self.buf[position+2])
        val = (val << 8) | ord(self.buf[position+3])
        return val

    def appendOctet(self, c):
        self.buf.append(chr(c))

    def appendInteger(self, I):
        c4 = [0,1,2,3]
        c4[0] = chr((I >> 24) & 0xFF)
        c4[1] = chr((I >> 16) & 0xFF)
        c4[2] = chr((I >> 8 ) & 0xFF)
        c4[3] = chr(I & 0xFF)
        self.buf.append(''.join(c4))

    def __str__(self):
        return "headerLen:%d" % len(self.buf)

class Packet:
    def __init__(self, s):
        self.header = None
        self.header_len = 0
        self.payload = None
        self.setBuf(s)

    def setBuf(self, s):
        self.buf = s
        if self.buf:
            self.getHeader()
            self.getPayload()

    def getHeader(self):
        if self.header:
            return self.header
        self.header = PacketHdr()
        self.header_len = ord(self.buf[0])
        self.header.setBuf(self.buf[1:1+self.header_len]) 
        return self.header

    def setHeader(self, header):
        self.header = header

    def setPayload(self, payload):
        self.payload = payload

    def getPayload(self, _size=-1):
        if self.payload:
            return self.payload
        #print 'getPayload::buf len:',len(self.buf)
        _index = 1 + self.header.getSize()
        if _index >= len(self.buf):
            self.payload = None
            return None
        if _size < 0:
            _size = len(self.buf) - _index
        self.payload = self.buf[_index: _index+_size]
        return self.payload

    def send(self, _sock, _addr):
        sbuf = [ chr(self.header.getSize()), self.header.getData() ]
        if self.payload:
            sbuf.append(self.payload)
        _send_data = ''.join(sbuf)
        #print 'sendto len:',len(_send_data), _send_data
        _sock.sendto( _send_data , _addr)

    def __str__(self):
        return str(self.getHeader())+"payload len:"# % ( self.payload ? len(self.payload):0)

if __name__ == '__main__':
    '''
    import socket
    server = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
    server.bind( ('', 17800) )  
    print 'udpserver start'
    headerOffset = 1
    while True:
        data,(addr, rport) = server.recvfrom( 1024 )
        print data,addr,rport
        #for i in range(16):
        #    print 'byte:',ord(data[i])
        ph = Packet(data).getHeader()
        print ph.getOctet(0),ph.getOctet(1),'int:',ph.getIntegerInfo(2)
    '''

    ph = PacketHdr()
    ph.appendInteger(123)
    ph.appendOctet('A')
    ph.appendInteger(88)
    ph.appendOctet('B')

    print 'len',ph.getSize()

    ph2 = PacketHdr()
    ph2.setBuf(ph.getData())
    print ph2.getIntegerInfo(0),ph2.getOctet(4)
    print ph2.getIntegerInfo(5),ph2.getOctet(9)

