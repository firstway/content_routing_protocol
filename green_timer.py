
import gevent
from gevent import Greenlet

class Timer(Greenlet):
    def __init__(self, interval):
        Greenlet.__init__(self)
        self.func_list = []
        self.interval = interval
        self.running = False

    def add(self, _func):
        self.func_list.append(_func)
        return self

    def _run(self):
        self.running = True
        while self.running:
            for _func in self.func_list:
                _func()
            gevent.sleep(self.interval)



if __name__  == '__main__':
    def F1():
        print "fff1"

    def F2():
        print "fff2"

    tt = Timer(3)
    tt.add(F1)
    tt.add(F2)

    tt.run()
