

import sys

import gevent
import gevent.select
from gevent import Greenlet
from gevent import socket
from gevent.queue import Queue

import connection 
from connection import Packet,PacketHdr
from green_timer import Timer
from config  import Config
from  router import Interface 

def ginput(prompt):
    sys.stdout.write( prompt )
    sys.stdout.flush()
    gevent.select.select([sys.stdin], [], [])
    return sys.stdin.readline()


class Host:
    def __init__(self, host_id, my_addr, router_addr):
        self.running = False
        self._id = host_id
        self.my_addr = my_addr
        self.router_addr = router_addr
        self.recv_q = Queue()
        self.iface = Interface(0, (my_addr, router_addr, 'R'), self.recv_q)
        self.contents = {}

    def addContent(self, cid, c):
        self.contents[cid] = c 

    
    def getContent(self, cid):
        if cid in self.contents:
            return self.contents[cid]
        return None

    def announce(self):
        for cid, c in self.contents.items():
            ph = PacketHdr()
            ph.appendOctet(2)#0x02
            ph.appendOctet(cid)#content id
            ph.appendOctet(0)#hop
            pack = Packet(None)
            pack.setHeader(ph)
            #print 'announce',cid,c
            self.iface.send(pack)

    def _build_send_response(self, header):
        #_type = header.getOctet(0)
        cid = header.getOctet(1)
        host_id = header.getOctet(2)
        if cid not in self.contents:
            print 'error: can NOT find content with id=',cid
            return None
        _content = self.contents[cid]
        #print '_build_send_response:: content',_content
        ph = PacketHdr()
        ph.appendOctet(1)#0x01
        ph.appendOctet(cid)#content id
        ph.appendOctet(host_id)#host id
        ph.appendOctet(len(_content))# size of payload
        pack = Packet(None)
        pack.setHeader(ph)
        pack.setPayload(_content)
        self.iface.send(pack)
    
    def _deal_content(self, header, pack):
        cid = header.getOctet(1)
        host_id = header.getOctet(2)
        payload_size = header.getOctet(3)
        _content = pack.getPayload(payload_size)
        print 'get content','(len',payload_size,'):',_content
        #print 'content:',len(_content),_content

    def run(self):
        self.running =  True

        self.iface.start()
        t1 = Timer(10).add(self.announce)
        t1.start()

        gevent.spawn( self._process_loop )
        self._cmd_loop()
        self.iface.running = False

    def _process_loop(self):
        while self.running:
            from_if,pack = self.recv_q.get()
            #print 'from',from_if,pack
            #self.if_list[1].send(pack)
            header = pack.getHeader()
            _type = header.getOctet(0)
            if _type == connection.MSG_C_REQ:
                self._build_send_response(header)
            elif _type == connection.MSG_C_RESP:
                self._deal_content(header, pack)
            else:
                print 'error packet type:',_type

    def request(self, cid):
        ph = PacketHdr()
        ph.appendOctet(0)#0x00
        ph.appendOctet(cid)#content id
        ph.appendOctet(self._id)#host id
        pack = Packet(None)
        pack.setHeader(ph)
        self.iface.send(pack)

    def _cmd_loop(self):
        while True:
            s = ginput('Add content or quit:')
            s = s.strip()
            if s == 'quit':
                break
            if len(s) > 1 and s[0] == '?':
                self.request(int(s[1:]))
            s = s.split('=')
            if len(s) > 1 and s[0].isdigit():
                self.addContent(int(s[0]), ''.join(s[1:]))
        self.running = False



if __name__ == '__main__':
    '''
    '''
    import sys
    if len(sys.argv) < 3:
        print "usage: %s config_file HOST_ID"
        sys.exit(1)
    config_file = sys.argv[1]
    host_id = int(sys.argv[2])

    conf = Config(config_file)
    conf.load()
    iface_conf = conf.get_host_conf(host_id)

    if not iface_conf:
        print "can NOT get conf for host",host_id
        sys.exit(1)

    H = Host(host_id, *iface_conf)
    #H.addContent(2, 'asasdasd')
    H.run()

