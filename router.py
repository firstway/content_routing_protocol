

from time import *


import gevent
from gevent import Greenlet
from gevent import socket
from gevent.queue import Queue

import connection 
from connection import Packet
from green_timer import Timer
from config  import Config



class Interface(gevent.Greenlet):
    def __init__(self, _id, config, recv_q):
        Greenlet.__init__(self)
        self.myid = _id
        self.listen_addr, self.remote_addr, self.to = config
        self.recv_q = recv_q
        self.send_q = Queue()
        self.running = False

    def __str__(self):
        return "iface:%d" % (self.myid)

    def __repr__(self):
        return self.__str__()

    def send(self, packet):
        self.send_q.put(packet)

    def _send_loop(self):
        self.s_sock = self.lsock
        while self.running:
            #send packets
            pk = self.send_q.get()
            #print self.myid,'send',pk,'to',self.remote_addr
            pk.send(self.s_sock, self.remote_addr)

    def _run(self):
        self.running = True

        self.lsock = socket.socket( socket.AF_INET, socket.SOCK_DGRAM )
        self.lsock.bind( self.listen_addr )  
        #print 'lsock',self.lsock
        gevent.spawn(self._send_loop)
        while self.running:
            #recv packets
            data,_remote = self.lsock.recvfrom( 1600 )
            if data:
                #print 'recvfrom len',len(data)
                self.recv_q.put( (self, Packet(data)) )


class RoutingTable:
    verbose = 1
    def __init__(self):
        #content ID is the key
        #value: iface, hop, expire
        self.table = {}

    def addItem(self, cid, value):
        isUpdate = False
        if cid in self.table:
            _v = self.table[cid]
            #if new one's hop is better or old one is expired
            if _v['hop'] > value['hop'] or _v['exp'] < time():
                self.table[cid] = value
                isUpdate = True
        else:
            self.table[cid] = value
            isUpdate = True
        if isUpdate and RoutingTable.verbose:
            print 'add item in RoutingTable: contend id',cid,value
        return isUpdate

    def getItem(self, cid):
        if cid in self.table:
            if self.table[cid]['exp'] < time():#expired
                return None
            return self.table[cid]
        return None


ROUTING_TABLE_TIMEOUT = 10


class PendingTable:
    verbose = 1
    def __init__(self):
        self.pending = {}

    def addRequest(self, cid, host_id, _v):
        _k = "%d|%d" % (cid, host_id)
        isUpdate = False
        if _k in self.pending:
            if _v['exp'] > self.pending[_k]['exp']:#update
                self.pending[_k] = _v
                isUpdate = True 
        else:
            self.pending[_k] = _v
            isUpdate = True 
        if isUpdate and PendingTable.verbose:
            print 'add item in PendingTable: contend id',cid,host_id,_v

    def cleanExpired(self):
        #print 'cleaning expired in pending....'
        for _k,_v in self.pending.items():
            if _v['exp'] > time():#expired
                if PendingTable.verbose:
                    print 'cleaning expired in pending',_k,_v
                del self.pending[_k]

    def getReturnIFace(self, cid, host_id):
        _k = "%d|%d" % (cid, host_id)
        if _k not in self.pending:
            return None
        _if = self.pending[_k]['if']
        del self.pending[_k]
        return _if


class Router:
    def __init__(self, interface_conf_list):
        self.recv_q = Queue()
        self.routing_table = RoutingTable()
        self.pending_table = PendingTable()
        self.timer_list = []
        self.if_list = []
        i = 0
        for cf in interface_conf_list:
            i += 1
            self.if_list.append( Interface(i, cf, self.recv_q) )
        #set some timers to clean 
        t1 = Timer(3).add(self.pending_table.cleanExpired)
        t1.start()
        self.timer_list.append(t1)

    def run(self):
        for iface in self.if_list:
            iface.start()
        self._process_loop()

    def _process_loop(self):
        while True:
            from_if,pack = self.recv_q.get()
            #print 'from',from_if,pack
            #self.if_list[1].send(pack)
            header = pack.getHeader()
            _type = header.getOctet(0)
            if _type == connection.MSG_C_REQ:
                self._forward(pack, from_if)
            elif _type == connection.MSG_C_RESP:
                self._forward_back(pack, from_if)
            elif _type == connection.MSG_C_UPDATE:
                self._process_routing_table(pack, from_if)
            else:
                print 'error packet type:',_type

    def _propagate(self, packet, _iface):
        #send this packet to other interfaces
        # except the hosts
        for _if in self.if_list:
            if _if is not _iface and _if.to == 'R':
                _if.send(packet)

    def _process_routing_table(self, packet, _iface):
        #recv update packet
        header = packet.getHeader()
        _type = header.getOctet(0)
        _cid = header.getOctet(1)
        _hop = 1+ header.getOctet(2)
        _v = {  'if' : _iface,
                'hop': _hop,
                'exp': time() + ROUTING_TABLE_TIMEOUT,
        }
        updated = self.routing_table.addItem(_cid, _v)
        header.setOctet(2, _hop)
        if updated:#only if new item, we can propagate it
            self._propagate(packet, _iface)

    def _forward(self, packet, _iface):
        #iquery the routing table
        header = packet.getHeader()
        _type = header.getOctet(0)
        _cid = header.getOctet(1)
        _host = header.getOctet(2)
        routing_info = self.routing_table.getItem(_cid)
        if not routing_info:
            print 'error: can not find next hop for',packet
            return 
        to_iface = routing_info['if']
        if _iface.myid == to_iface.myid:
            print 'error: loop for forwarding ',packet
            return 
        #add info to PendingTable
        _v = {  'if' : _iface,
                'exp': time() + ROUTING_TABLE_TIMEOUT,
        }
        self.pending_table.addRequest(_cid, _host, _v)
        #forwarding by routing_table
        print 'forwarding request',_cid, _host,'to',to_iface
        to_iface.send(packet)


    def _forward_back(self, packet, _iface):
        #forward the resp packet to host
        header = packet.getHeader()
        _type = header.getOctet(0)
        _cid = header.getOctet(1)
        _host = header.getOctet(2)
        to_iface = self.pending_table.getReturnIFace(_cid, _host)
        if not to_iface:
            print 'error: can not find path in pending for',packet
            return 
        if _iface.myid == to_iface.myid:
            print 'error: loop for forwarding back',packet
            return 
        to_iface.send(packet)

if __name__ == '__main__':
    '''
    '''
    import sys
    if len(sys.argv) < 3:
        print "usage: %s config_file router_ID"
        sys.exit(1)
    config_file = sys.argv[1]
    router_id = int(sys.argv[2])

    conf = Config(config_file)
    conf.load()
    interface_conf_list = conf.get_interface_conf_list(router_id)

    if not interface_conf_list:
        print "can NOT get conf for router",router_id
        sys.exit(1)

    for c in interface_conf_list:
        print c 
    r = Router(interface_conf_list)
    RoutingTable.verbose = 1
    r.run()


