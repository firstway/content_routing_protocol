Implementation of a simple "content routing" protocol.
the network service involves delivery of named content files.
In contrast to an IP network where packets are routed to a destination address (network port),
a content network goes one step further by directly fetching a content file (as identified by a unique identifier) from the nearest location at which it is currently stored. Content may
be stored in network-attached devices such as client PCs or servers as in the current Internet.